# System requirements

## Layout

The system UI should have the following sections:

### Students list
* Section should show a list of all students that have been registered on the system.
* The list should show the students' first name, last name and grade year.
* The list should be sorted by first name on ascending order.

### Register students
* Should show text inputs for entering the following student information:
    * First name
    * Last name
    * Grade year (1st grade to 6th grade)
* Should show an "Add from file" option.

### Courses list
* Should show a list of all courses that have been registered on the system.
* The list should show the course name, level and price.
*The list should be sorted by course name on ascending order.

### Register course
* Should show text inputs for entering the following course information:
    * Course name
    * Price
    * Level (Beginner, Intermediate or Advanced)


## Use cases

```
Feature: Register Students
    Scenario: Add a student manually
        Given I am on the "Register students" section
        When I enter the student information
        Then the student's information is saved
        And the student info is added to the "Student list" section

    Scenario: Add multiple students from file
        Given I am on the "Register students" section
        When I select "Add from file" option
        Then I can select a CSV file with students infomation from my computer
        When I select the file
        Then the students' information is saved
        And the students' info is added to the "Student list" section

Feature: Register Courses
    Scenario: Add a course
        Given I am on the "Register course" section
        When I course information
        Then the course information is saved
        And the course info appears on the "Courses list" section

```
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from .models import Course


class LogInForm(AuthenticationForm):
    username = forms.CharField(max_length=25)
    # password = forms.CharField(max_length=25)

    class Meta:
        model = User
        fields = ['username', 'password']

    def send_email(self):
        # send email using the self.cleaned_data dictionary
        pass


class RegisterForm(UserCreationForm):
    username = forms.CharField(max_length=25)
    first_name = forms.CharField(max_length=25)
    last_name = forms.CharField(max_length=25)
    email = forms.EmailField(max_length=45)
    grade = forms.IntegerField()

    class Meta:
        model = User
        fields = [
            'username', 'first_name', 'last_name',  'email', 'password1', 'password2',
        ]

    # def save(self, commit=True):
    #     user = super(RegisterForm, self).save(commit=False)
    #     user.email = self.cleaned_data["email"]
    #     if commit:
    #         user.save()
    #     return user


class ProfileForm(forms.ModelForm):

    # def __init__(self):
    #     super(ProfileForm, self).__init__()
    #     return

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            ]


class CreateCourseForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = [
            'id',
            'name',
            'subject',
            'description',
            'level',
            # 'publication_date',
            'start_date',
            'price',
            'occupancy_limit',
            'teacher',
        ]

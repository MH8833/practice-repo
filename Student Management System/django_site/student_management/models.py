from django.db import models
from django.contrib.auth.models import AbstractBaseUser, User
from django.urls import reverse


class Teacher(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=45)
    title = models.CharField(max_length=45)
    bio = models.CharField(max_length=400)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)


class Subject(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=255)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)


class Course(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=45)
    description = models.TextField(max_length=400)
    level = models.CharField(max_length=25)
    publication_date = models.DateField(auto_now=True)
    start_date = models.DateTimeField()
    price = models.FloatField()
    occupancy = models.IntegerField(default=0)
    occupancy_limit = models.IntegerField(default=50)

    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)

    def get_absolute_url(self):
        return reverse('student_management:course', kwargs={'pk': self.id})


class Student(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    email = models.EmailField(max_length=45)
    grade = models.IntegerField()

    courses = models.ManyToManyField(Course)

    def __str__(self):
        return str(self.name) + " " + str(self.last_name)









from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
# from django.views.generic import CreateView

from .models import Course, Student
from django.utils import timezone
from django.contrib.auth.models import User
from .forms import RegisterForm, ProfileForm, LogInForm, CreateCourseForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth

import pdb


class IndexView(generic.ListView):
    template_name = 'student_management/index.html'
    context_object_name = 'courses_list'

    def get_queryset(self):
        return Course.objects.all().order_by('start_date')

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['students'] = Student.objects.all()
        context['now'] = timezone.now()

        if self.request.user.is_authenticated:
            student = Student.objects.get(user=self.request.user.id)
            if student is not None:
                context['student_id'] = student.id
                context['user_id'] = self.request.user.id

                # if self.request.user.is_authenticated:
        #     context['user_name'] = self.request.user.username
        #     context['user_id'] = self.request.user.id
        # else:
        #     context['user_name1'] = "WHY WHY WHYYYYY"
        return context


class CourseView(generic.DetailView):

    template_name = 'student_management/course_details.html'
    context_object_name = 'course'
    model = Course

    def get_queryset(self):
        return Course.objects

    def get_context_data(self, **kwargs):
        context = super(CourseView, self).get_context_data(**kwargs)
        context['loggedIn'] = False
        context['student_has_course'] = False
        if self.request.user.is_authenticated:
            context['user'] = self.request.user.username
            context['loggedIn'] = True
            # student = Student.objects.get(user=self.request.user.id)
            #
            # course = student.courses.through.objects.get(course_id=kwargs.get('pk'), student_id=student.id)
            # if course is not None:
            #     context['student_has_course'] = True
        else:
            context['user'] = "not logged in"
        print("context", context)
        return context

    # def get(self, request, pk):
    #     if self.request.user.is_authenticated:
    #         student = Student.objects.get(user=self.request.user.id)
    #         print("course_id", pk)
    #         print("student_id", student.id)
    #         course = student.courses.through.objects.get(course_id=pk, student_id=student.id)
    #         if course is not None:
    #             context['student_has_course'] = True
    #             self.extra_context()

    def post(self, request, pk):
        # return self.subscribe_un_to_course(request, pk, subscribe=True)
        if self.request.user.is_authenticated:
            student = Student.objects.get(user=self.request.user.id)
            course = Course.objects.get(id=pk)
            # student = get_object_or_404(Student, id=self.request.user.id)
            if course.occupancy < course.occupancy_limit:
                student.courses.add(course.id)
                student.save()
                course.occupancy += 1
                course.save()
            else:
                return show_base_error_message(request, title="Register error", message="This course is full")

            # pdb.set_trace()
            return HttpResponseRedirect(reverse_lazy('student_management:student_courses'))
        else:
            return HttpResponseRedirect(reverse_lazy('student_management:login'))

    # def delete(self, request, pk):
    #     self.subscribe_un_to_course(request, pk, subscribe=False)

    # def subscribe_un_to_course(self, request, course_id, subscribe):
    #     if self.request.user.is_authenticated:
    #         student = Student.objects.get(user=self.request.user.id)
    #         course = Course.objects.get(id=pk)
    #         # student = get_object_or_404(Student, id=self.request.user.id)
    #
    #         if subscribe:
    #             # student1 = get_object_or_404(Student, =title)
    #             student.courses.add(course.id)
    #             student.save()
    #         else:
    #             student_course = student.courses.through.objects.filter(student_id=student.id, course_id=course.id)
    #             student_course.delete()
    #
    #         # pdb.set_trace()
    #         return HttpResponseRedirect(reverse_lazy('student_management:student_courses'))
    #     else:
    #         return HttpResponseRedirect(reverse_lazy('student_management:login'))


class LogInView(generic.FormView):
    template_name = 'student_management/users/login.html'
    form_class = LogInForm
    user_id = 0
    success_url = reverse_lazy('student_management:index')

    def get_success_url(self):
        # return reverse_lazy('student_management:profile', args=(self.user_id,))
        return reverse_lazy('student_management:index')

    def get(self, request):
        if request.user.is_authenticated:
            return show_base_error_message(request, title="login error", message="You are already logged in")

        # return render(request, template_name=self.template_name)      #correct but missing super functionality
        return super().get(request)

    def post(self, request):

        if request.user.is_authenticated and request.POST.get('username') == request.user.username:
        # if request.user.is_authenticated:
            return show_base_error_message(request, title="login error", message="You are already logged in")

        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse_lazy('student_management:profile', args=(user.id,)))
        else:
            return show_base_error_message(self.request, title="login error", message="invalid username or password")


def log_out(request):
    if request.user.is_authenticated:
        logout(request)
        #TODO: ADD success message
    return HttpResponseRedirect(reverse_lazy('student_management:index'))


class RegisterView(generic.CreateView):
    template_name = 'student_management/users/register.html'
    form_class = RegisterForm
    # user_id = User.objects.get(username=User.username).pk
    # success_url = reverse_lazy('student_management:profile', args=(user_id,))
    success_url = reverse_lazy('student_management:index')
    # model = User

    def form_valid(self, form):
        if self.request.user.is_authenticated:   #TODO: add message that is alreadi logedin'
            return HttpResponseRedirect(reverse_lazy('student_management:index'))
            # return HttpResponseRedirect(reverse_lazy('student_management:profile', kwargs={'pk': self.pk}))

        # form = RegisterForm(request.POST)
        # if form.is_valid():
        #     form.save()
        super().form_valid(form)
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1']) #request.POST['username'], password=request.POST['password1'])
        login(self.request, user)
        student = Student()
        student.name = form.cleaned_data.get('first_name')
        student.last_name = form.cleaned_data.get('last_name')
        student.email = form.cleaned_data.get('email')
        student.grade = form.cleaned_data.get('grade')
        student.user = user
        student.save()
        return HttpResponseRedirect(reverse_lazy('student_management:profile', kwargs={'pk': user.id}))
        # else:
        #     #TODO: show error message
        #     return super().form_valid(form)


class ProfileView(generic.UpdateView):
    model = User
    form_class = ProfileForm
    success_url = reverse_lazy('student_management:index')
    template_name = 'student_management/users/profile.html'


class StudentCourses(generic.ListView):
    model = Course
    template_name = 'student_management/student_curses.html'
    context_object_name = 'courses_list'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        student = Student.objects.get(user=self.request.user.id)
        student_courses = student.courses.through.objects.filter(student_id=student.id)
        # student_courses = student.courses.through.objects.filter(student_id=student.id, course_id=course.id)
        courses_list = []
        for sc in student_courses:
            courses_list.append(Course.objects.get(id=sc.course_id))

        data['courses_list'] = courses_list

        data['student'] = Student.objects.get(user=self.request.user.id)
        return data

    def get(self, request):
        if not request.user.is_authenticated:
            return show_base_error_message(request, title="Error", message="You are not logged in")

        return super().get(request)

    def post(self, request):   # TODO: try delete metod (not used because csrf error)
        course_id = request.POST.get('course_id')
        student = Student.objects.get(user=self.request.user.id)    # TODO: Try to get form context
        student_course = student.courses.through.objects.filter(student_id=student.id, course_id=course_id)
        course = Course.objects.get(id=course_id)
        course.occupancy -= 1
        course.save()
        print(student_course)
        student_course.delete()

        return HttpResponseRedirect(reverse_lazy('student_management:student_courses'))


class CourseCreateView(LoginRequiredMixin, generic.CreateView):
    form_class = CreateCourseForm
    # success_url = reverse_lazy('student_management:index')
    template_name = 'student_management/course_form.html'
    model = Course

    # def form_valid(self, form):
    #     form.instance.publication_date = timezone.now()
    #     return super().form_valid(form)


def show_base_error_message(request, title=None, message=None):
    context = {'title': title, 'message': message}
    return render(request, "student_management/base.html", context=context)







    # def signup(request):
    #     if request.user.is_authenticated:
    #         return HttpResponseRedirect(reverse_lazy('student_management:index'))
    #
    #     if request.method == 'POST':
    #         form = UserCreationForm(request.POST)
    #
    #         if form.is_valid():
    #             form.save()
    #             username = form.cleaned_data['username']
    #             password = form.cleaned_data['password1']
    #             user = authenticate(username=username, password=password)
    #             login(request, user)
    #             return HttpResponseRedirect(reverse_lazy('student_management:index'))
    #
    #         else:
    #             return HttpResponseRedirect(reverse_lazy('student_management:login'))
    #             # return render(request, 'student_management/users/register.html', {'form': form})
    #
    #     else:
    #         form = UserCreationForm()
    #         return render(request, 'student_management/users/register.html', {'form': form})





    # all_models_dict = {
    #     "template_name": "contacts/index.html",
    #     "queryset": Individual.objects.all(),
    #     "extra_context": {"role_list": Role.objects.all(),
    #                       "venue_list": Venue.objects.all(),
    #                       # and so on for all the desired models...
    #                       }
    # }




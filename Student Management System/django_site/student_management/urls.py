from django.urls import path, include
from . import views

app_name = 'student_management'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('register/course/', views.CourseCreateView.as_view(), name='register_course'),
    path('<int:pk>/course/', views.CourseView.as_view(), name='course'),
    path('<int:pk>/profile/', views.ProfileView.as_view(), name='profile'),
    path('my/courses/', views.StudentCourses.as_view(), name='student_courses'),
    path('accounts/', include('django.contrib.auth.urls'),),
    path('register/', views.RegisterView.as_view(), name='register'),
    # path('register/', views.signup, name='register'),
    path('login/', views.LogInView.as_view(), name='login'),
    path('logout/', views.logout, name='log_out'),
]


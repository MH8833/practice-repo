#!/bin/bash

STACK_NAME="jv-ecs-cluster-stack"
STACK_TEMPLATE="file://cluster-template.yml"
AWS_PROFILE="jv"
AWS_REGION="us-east-1"

aws cloudformation create-stack \
  --stack-name "$STACK_NAME" \
  --template-body "$STACK_TEMPLATE" \
  --capabilities CAPABILITY_NAMED_IAM \
  --profile "$AWS_PROFILE" \
  --region us-east-1

#!/bin/bash

VERSION="v0.0.3"

STACK_NAME="student-management-stack"
STACK_TEMPLATE="file://student-management-cf-template.yml"
AWS_PROFILE="jv"
AWS_REGION="us-east-1"

# Build image with name and version
docker build -t "student-management:$VERSION" ../.

# Tag built image with AWS image repository using ARN
docker tag "student-management:$VERSION" "680291540017.dkr.ecr.us-east-1.amazonaws.com/student-management-system:$VERSION"

# Login to ECR (this command is different for AWS CLI V2)
$(aws ecr --profile jv --region us-east-1 get-login --no-include-email)

# Push image to AWS image repository
docker push "680291540017.dkr.ecr.us-east-1.amazonaws.com/student-management-system:$VERSION"

# Deploy CloudFormation stack
aws cloudformation create-stack \
#aws cloudformation update-stack \
  --stack-name "$STACK_NAME" \
  --template-body "$STACK_TEMPLATE" \
  --parameters ParameterKey=ImageUrl,ParameterValue="680291540017.dkr.ecr.us-east-1.amazonaws.com/student-management-system:$VERSION" \
  --capabilities CAPABILITY_NAMED_IAM \
  --profile "$AWS_PROFILE" \
  --region us-east-1
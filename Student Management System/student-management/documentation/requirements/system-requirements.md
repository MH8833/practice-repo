# System requirements

## Requirements description

The system should have the following features:

* There are three type of users, admin, teacher and student.
* Admin and teacher can add courses.
* Admin can add students, either one by one or in bulk (for example, from a text file).
* Students can enroll themselves to courses.
* Students can be enrolled to a maximum of N courses at a time.
* Students can unenroll from a course.
* Courses have a limited capacity, meaning that only X number of students can be enrolled to a course at a time.
* Students can only enroll to a course if they have enrolled to its prerequired courses (de este no muy estoy seguro, pero creo que estaría bien agregar algo así).
* There are two types of courses: 1) ** static ** can always be active 2) ** active teaching ** have a limited capacity and a start and end date.


### Notifications
- The system should notified the student when a course he is subscribed starts.
- The system should notified when a course he is subscribed is modified


## Layout

The system UI should have the following sections:

### main page "home"
* Should show basic user information (name, status,)
* Should show diferent courses list (active courses, recomended courses, courses by category[admine defined])
* Should display Course search funtionality

### Students list (admin)
* Section should show a list of all students that have been registered on the system.
* The list should show the students' first name, last name and grade year.
* The list should be sorted by first name on ascending order.

### Register students (admin)
* Should show ~~text~~ inputs for entering the following student information:
  * First name
  * Last name
  * Grade year (1st grade to 6th grade)
* Should show an "Add from file" option.


### Login (admin, student, teacher)
* Should show the same login view for all users
* After login should set apropiate view for diferent users

### Logout (admin, student, teacher)
* Should ask for confirmation: "login out from (user name), please confirm"

### Courses list (admin, student)
* Should show a list of all courses that have been registered on the system.
* The list should show the course name, level and price.
* The list should be sorted by course name on ascending order.

### Course search results (admin)
* Courses can be search by topic, key words, teacher, course status
* Should implement Courses list View

### 

### Recommended courses list (student)
* Should show a list of courses related to previous courses the user has enrolled

### Register course (admin)
* Should show ~~text~~ inputs for entering the following course information:
  * Course name
  * ~~Price~~ Capacity
  * Level (Beginner, Intermediate or Advanced)
  


### Course enrollment (student)
* The program should show controls to allow a student to enroll himself to a course.

### User selection (admin, student)
* The program should show controls to change the active user (from admin to a registered student and back).'




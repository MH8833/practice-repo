
import views.MainView;
import main.MainLogicController;


public class Main {


  public Main(){

    MainLogicController mainController =  new MainLogicController();
    MainView mainView = new MainView(mainController);
    mainView.setVisible(true);
  }

  public static void main(String[] args) {
    new Main();
  }
}

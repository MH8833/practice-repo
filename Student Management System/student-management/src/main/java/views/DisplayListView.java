package views;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JFrame;


public class DisplayListView {

    JFrame mainFrame;
    JTable table;

    public DisplayListView(String[][] data, String[] columns)
    {
        mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainFrame.setTitle("Students List");

        table = new JTable(data, columns);
        table.setBounds(30, 40, 200, 300);
        JScrollPane jsp = new JScrollPane(table);
        mainFrame.add(jsp);
        mainFrame.setSize(500, 200);
        mainFrame.setVisible(true);
    }

}
package views;

import main.MainLogicController;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MainView extends JFrame{

    public MainView(MainLogicController mainController){
        initGUI(mainController);
    }

    private void initGUI(MainLogicController mainController) {
        //JFrame frame = new JFrame("Student management system");
        setTitle("Student Management System");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 450));
        setLocationByPlatform(true);

        JPanel panel = new JPanel();
        add(panel);

        JButton showStudentsButton = new JButton("Students List");
        showStudentsButton.setBounds(10, 160, 80, 25);
        panel.add(showStudentsButton);

        showStudentsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                getStudentsList();
                mainController.getStudentsList();
            }
        });

        JButton showCoursesButton = new JButton("Courses List");
        showCoursesButton.setBounds(10, 160, 80, 25);
        panel.add(showCoursesButton);

        showCoursesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainController.getCourseList();
            }
        });

        JButton registerStudentButton = new JButton("Register Student");
        registerStudentButton.setBounds(10, 160, 80, 25);
        panel.add(registerStudentButton);

        registerStudentButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                StudentRegistrationView regForm = new StudentRegistrationView(mainController);
                regForm.setVisible(true);
            }
        });

        pack();
//        setVisible(true);
    }

}

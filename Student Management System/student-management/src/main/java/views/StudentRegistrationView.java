package views;

import containers.Student;
import database.DataBaseManager;
import main.MainLogicController;
import utilities.Utilities;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class StudentRegistrationView extends JFrame {
    JTextField mNameField;
    JTextField mLastNameField;
    JTextField mEmailFiled;
    JTextField mGradeField;


    public StudentRegistrationView(MainLogicController mainController) {

        createGui(mainController);
    }

    private void createGui(MainLogicController mainController){
//        mFrame = new JFrame("Student Registration");
//        mFrame.setSize(350, 250);
//        mFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        JPanel panel = new JPanel();
//        mFrame.add(panel);
//        mFrame.setVisible(true);

        setTitle("Student Registration");
        setSize(350, 250);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel panel = new JPanel();
        add(panel);

        panel.setLayout(null);
        JLabel userLabel1 = new JLabel("First Name");
        userLabel1.setBounds(10, 20, 80, 25);
        panel.add(userLabel1);
        JLabel userLabel2 = new JLabel("Last Name");
        userLabel2.setBounds(10, 50, 100, 25);
        panel.add(userLabel2);
        JLabel userLabel3 = new JLabel("Email ID");
        userLabel3.setBounds(10, 110, 80, 25);
        panel.add(userLabel3);
        JLabel userLabel4 = new JLabel("Grade");
        userLabel4.setBounds(10, 80, 80, 25);
        panel.add(userLabel4);
        mNameField = new JTextField(20);
        mNameField.setBounds(100, 20, 165, 25);
        panel.add(mNameField);
        mLastNameField = new JTextField(20);
        mLastNameField.setBounds(100, 50, 165, 25);
        panel.add(mLastNameField);
        mGradeField = new JTextField(20);
        mGradeField.setBounds(100, 80, 165, 25);
        panel.add(mGradeField);
        mEmailFiled = new JTextField(20);
        mEmailFiled.setBounds(100, 110, 165, 25);
        panel.add(mEmailFiled);
        JButton registerButton = new JButton("Register");
        registerButton.setBounds(10, 160, 100, 25);
        panel.add(registerButton);
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Student student = getFieldsData();
                if(Utilities.validateEmail(student.email)) {
                    if (mainController.registerStudent(student)) {
                        JOptionPane.showMessageDialog(null, new String("Registration successful"));
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, new String("Registration failed"));
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, new String("Registration failed, incorrect email format"));
                }
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setBounds(100, 160, 80, 25);
        panel.add(cancelButton);
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    private Student getFieldsData(){
        Student student = new Student();
        student.name = mNameField.getText();
        student.lastname = mLastNameField.getText();
        student.grade = Integer.parseInt(mGradeField.getText());
        student.email = mEmailFiled.getText();

        return student;
    }

}
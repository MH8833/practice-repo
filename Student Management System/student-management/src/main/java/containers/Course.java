package containers;

public class Course {
    public int idCourse;
    public String name;
    public int idTeacher;
    public String description;
    public String level;
    public float price;
    public int idSubject;
}

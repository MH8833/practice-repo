package main;

import database.DataBaseManager;

import containers.Course;
import containers.Student;

import views.DisplayListView;
import views.StudentRegistrationView;

import javax.swing.*;
import java.util.ArrayList;


public class MainLogicController {
    DataBaseManager mDatabaseManager;

    public MainLogicController(){
        mDatabaseManager = new DataBaseManager();



    }

    public void getStudentsList(){
        ArrayList<Student> students = mDatabaseManager.getStudents();
        int n = students.size();

        String [][] studentsTable = new String[n][3];
        int i =0;
        for (Student s : students) {

          studentsTable[i][0] = s.name;
          studentsTable[i][1] = s.lastname;
          studentsTable[i][2] = s.email;
          ++i;
        }
        String[] headers = {"Name","Last Name","Email"};

        DisplayListView studentsList = new DisplayListView(studentsTable,headers);
    }

    public void getCourseList(){
        ArrayList<Course> courses = mDatabaseManager.getCourses();

        int n = courses.size();
        String[][] listData = new String[n][5];
        int i = 0;
        for(Course c : courses){
            int j = 0;
            listData[i][j++] = c.name;
            listData[i][j++] = Integer.toString(c.idSubject);   //TODO: get subject name instead of id AND change for category?
            listData[i][j++] = c.level;
            listData[i][j++] = Integer.toString(c.idTeacher);   //TODO: fix get teacher name instead of id
            listData[i][j++] = Float.toString(c.price);
            ++i;
        }

        String[] labels = {"Name","Category","Level","Teacher","Price"};
        new DisplayListView(listData,labels);
    }

    public boolean registerStudent(Student student){
        return mDatabaseManager.insertStudent(student);
    }

}

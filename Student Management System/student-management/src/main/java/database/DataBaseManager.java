package database;

import com.mysql.cj.x.protobuf.MysqlxPrepare;
import containers.Course;
import containers.Student;
import utilities.Utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

import java.util.ArrayList;
import java.util.Properties;

public class DataBaseManager {
    private Connection mConnection;

    private static final String studentTable = "Student"; //CHECAR SI ES ESTATICO Y CARGARLO DEL ARCHIVO
    private static final String studentId = "idStudent";
    private static final String studentName = "name";
    private static final String studentLastName = "lastName";
    private static final String studentGrade = "grade";
    private static final String studentEmail = "email";

    private static final String courseTable = "Course";
    private static final String courseId = "idCourse";
    private static final String courseName = "name";
    private static final String courseIdTeacher = "Teacher_idTeacher";
    private static final String courseDescription = "description";
    private static final String courseLevel = "level";
    private static final String coursePrice = "price";
    private static final String courseIdSubject = "Subject_idSubject";

    private static final String subjectTable = "Subject";

    private static final String teacherTable;// = "Teacher";

    private static final String studentHasCourseTable = "Student_has_course";

    static{
        teacherTable = "something else";
    }

   public DataBaseManager() {
//        getTablesNames();     // can't assign value to final variable
        dataBaseInit();
    }

    private Properties getConnectionData() {

        Properties props = new Properties();

        try (FileInputStream in = new FileInputStream("src/main/resources/db_credentials.properties")) {
            props.load(in);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return props;
    }

    private void getTablesNames(){
        Properties props =  new Properties();
        try(FileInputStream in = new FileInputStream("src/main/resources/db_tables")){
            props.load(in);
        } catch(IOException ex){
            ex.printStackTrace();
            return;
        }
//        courseTable = props.getProperty("db.course");
//        studentTable = props.getProperty("db.student");
//        subjectTable = props.getProperty("db.subject");
//        teacherTable = props.getProperty("db.teacher");
//        studentHasCourseTable = props.getProperty("db.student_course");

    }

    private void dataBaseInit() {

        Properties properties = getConnectionData();
        try {
            mConnection = DriverManager.getConnection(properties.getProperty("db.url"),
                    properties.getProperty("db.user"),
                    properties.getProperty("db.password"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Connection successful");

        // get tables


    }
    // FIX: what to return
    public ArrayList<Student> getStudents(){

        String sqlSelect = "SELECT * FROM " + studentTable; // FIX: get from struct or something similar + Student.get();
        ArrayList<Student> students = new ArrayList<Student>();
        try{
            Statement statement = mConnection.createStatement();
            ResultSet result = statement.executeQuery(sqlSelect);

            while(result.next()){
                Student student = new Student();
                student.name = result.getString(studentName);
                student.lastname = result.getString(studentLastName);
                student.email = result.getString(studentEmail);
                student.id  = result.getInt(studentId);
                student.grade = result.getInt(studentGrade);

                students.add(student);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return students;
    }

    public ArrayList<Course> getCourses(){
       String sqlSelect = "SELECT * FROM " + courseTable;
        ArrayList<Course> courses = new ArrayList<Course>();
       try{
           Statement statement = mConnection.createStatement();
           ResultSet result = statement.executeQuery(sqlSelect);
           while(result.next()){
               Course course = new Course();
               course.idCourse = result.getInt(courseId);
               course.name = result.getString(courseName);
               course.description = result.getString(courseDescription);
               course.idSubject = result.getInt(courseIdSubject);
               course.idTeacher = result.getInt(courseIdTeacher);
               course.level = result.getString(courseLevel);
               course.price = result.getFloat(coursePrice);

               courses.add(course);
           }

       } catch (SQLException e) {
           e.printStackTrace();
       }

        return courses;
    }

    public boolean insertStudent(Student student){
        if(!Utilities.validateEmail(student.email))
        {
            return false;
        }
        boolean status = false;
        String insertQuery = "insert into student values(null,\"" + student.name + "\", "
                                                            + "\"" + student.lastname + "\", "
                                                            + "\"" + student.grade + "\", "
                                                            + "\"" + student.email + "\""
                                                            + ");";
//        String pStatement = "INSERT INTO student(id,name,lastname,grade,email) VALUES(?)";
        try{
            Statement statement = mConnection.createStatement();
//            PreparedStatement pst = mConnection.prepareStatement(pStatement);
//            pst.set
            statement.execute(insertQuery);
            status = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            status = false;
        }

        return status;
    }

}



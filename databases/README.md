To run the database containers use the following command:

```
docker-compose up
```

To connect to the MySQL web admin go to:
```
http://localhost:8080
```
Credentials:
```
username: root (default)
password: example (specified in docker-compose file)
```

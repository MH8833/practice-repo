#create database Student_Management_System;



SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Student_Management_System
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Student_Management_System` DEFAULT CHARACTER SET utf8 ;
USE `Student_Management_System` ;

-- -----------------------------------------------------
-- Table `Student_Management_System`.`Student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Student` (
  `idStudent` INT ZEROFILL NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `grade` TINYINT NOT NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`idStudent`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Student_Management_System`.`Teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Teacher` (
  `idTeacher` INT ZEROFILL NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `bio` VARCHAR(245) NULL,
  PRIMARY KEY (`idTeacher`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Student_Management_System`.`Subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Subject` (
  `idSubject` INT ZEROFILL NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `decription` VARCHAR(100) NULL,
  PRIMARY KEY (`idSubject`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Student_Management_System`.`Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Course` (
  `idCourse` SMALLINT ZEROFILL UNSIGNED NOT NULL,
  `name` VARCHAR(45) NULL,
  `Teacher_idTeacher` INT ZEROFILL NOT NULL,
  `description` TEXT(200) NULL,
  `level` ENUM("begginer", "intermidiate", "expert") NULL,
  `price` FLOAT NOT NULL,
  `Subject_idSubject` INT ZEROFILL NOT NULL,
  PRIMARY KEY (`idCourse`, `Teacher_idTeacher`),
  INDEX `fk_Course_Teacher1_idx` (`Teacher_idTeacher` ASC) VISIBLE,
  INDEX `fk_Course_Subject1_idx` (`Subject_idSubject` ASC) VISIBLE,
  CONSTRAINT `fk_Course_Teacher1`
    FOREIGN KEY (`Teacher_idTeacher`)
    REFERENCES `Student_Management_System`.`Teacher` (`idTeacher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Course_Subject1`
    FOREIGN KEY (`Subject_idSubject`)
    REFERENCES `Student_Management_System`.`Subject` (`idSubject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Student_Management_System`.`Student_has_Course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Student_has_Course` (
  `Student_idStudent` INT ZEROFILL NOT NULL,
  `Course_idCourse` SMALLINT ZEROFILL UNSIGNED NOT NULL,
  `enrollment_date` DATETIME NULL,
  PRIMARY KEY (`Student_idStudent`, `Course_idCourse`),
  INDEX `fk_Student_has_Course_Course1_idx` (`Course_idCourse` ASC) VISIBLE,
  INDEX `fk_Student_has_Course_Student_idx` (`Student_idStudent` ASC) VISIBLE,
  CONSTRAINT `fk_Student_has_Course_Student`
    FOREIGN KEY (`Student_idStudent`)
    REFERENCES `Student_Management_System`.`Student` (`idStudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Student_has_Course_Course1`
    FOREIGN KEY (`Course_idCourse`)
    REFERENCES `Student_Management_System`.`Course` (`idCourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Student_Management_System`.`purchase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Student_Management_System`.`Purchase` (
  `idpurchase` INT NOT NULL AUTO_INCREMENT,
  `purhcase_date` TIMESTAMP NOT NULL,
  `purchase_price` INT NOT NULL,
  PRIMARY KEY (`idpurchase`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

insert into student values(null,"ricardo","velarde",6,"ric@gmail.com");
insert into student values(null,"daniel","alvarez",5,"dan@gmail.com");
insert into student values(null,"mariana","boisson",1,"mariana@gmail.com");
insert into student values(null,"alejandra","figueroa",2,"figueroa@gmail.com");

insert into teacher values(12412,"carlos","jimenez","Computer engineer", "some acomplisments data");
insert into teacher values(12344,"james","smith","Dr. computer science",null);

insert into subject values(null,"programming","for programers :)");
insert into subject values(null,"data science","for data scientist :)");
insert into subject values(null,"self improvement","to improve yourself :P ");

insert into course values(21,"java for begginers",12412,"from zero to hero","begginer",25,1);
insert into course values(33,"c++ for begginers",12344,"from zero to hero","begginer",55,1);
insert into course values(12,"c# for begginers",12412,"from zero to hero","begginer",15,1);

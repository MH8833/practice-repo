//package com.project.SMS.controller;
//
//import com.project.SMS.UserManagement.RoleRepository;
//import com.project.SMS.UserManagement.User;
//import com.project.SMS.UserManagement.UserService;
//import com.project.SMS.service.CourseService;
//import com.project.SMS.service.StudentService;
//import com.project.SMS.service.TeacherService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Captor;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.security.Principal;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.doThrow;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//
//public class SMS_ControllerTest {
//  @Mock
//  private UserService userService;
//  @Mock
//  private CourseService courseService;
//  @Mock
//  private StudentService studentService;
//  @Mock
//  private TeacherService teacherService;
//  @Mock
//  private RoleRepository roleRepository;
//  @Mock
//  private Authentication auth;
//  @Mock
//  SecurityContext securityContext;
//
//  @Captor
//  ArgumentCaptor<User> userCaptor;
//
//  SMS_Controller controller;
//
//  @BeforeEach
//  void setUp() {
//    MockitoAnnotations.openMocks(this);
//    SecurityContextHolder.setContext(securityContext);
//    when(securityContext.getAuthentication()).thenReturn(auth);
//
//    controller = new SMS_Controller(userService, courseService, studentService, teacherService, roleRepository);
//  }
//
//  @Test
//  void homeAddsAuthUsernameToModelWhenContextIsAuthenticated() {
//    when(auth.isAuthenticated()).thenReturn(true);
//    when(auth.getName()).thenReturn("someUsername");
//
//    ModelAndView homeView = controller.home(new TestPrincipal(""));
//
//    assertEquals( "someUsername", homeView.getModel().get("username"));
//  }
//
//  @Test
//  void homeAddsEmptyUsernameToModelWhenContextIsNotAuthenticated() {
//    when(auth.isAuthenticated()).thenReturn(false);
//
//    ModelAndView homeView = controller.home(new TestPrincipal(""));
//
//    assertEquals( "", homeView.getModel().get("username"));
//  }
//
//  @Test
//  void processRegisterCallsRegisterUserOnServiceWithCorrectArgument() {
//    User user = new User();
//    user.setUserName("someName");
//
//    controller.processRegister(user);
//
//    verify(userService).registerUser(userCaptor.capture());
//
//    User userArgument = userCaptor.getValue();
//    assertEquals("someName", userArgument.getUserName());
//  }
//
//  @Test
//  void processRegister_ServiceReturnsRegisterFail_WhenServiceThrowsDuplicatedUserException() {
////    when(userService.registerUser(any())).thenThrow(RuntimeException.class);
//    doThrow(RuntimeException.class).when(userService).registerUser(any());
//
//    ModelAndView result = controller.processRegister(new User());
//
////    assertEquals("register_fail", result);
//  }
//
////  @Test
////  void registerToCourse_
//
////  @Test
////  void processRegisterServiceThrows() {
////    doThrow(DuplicatedUserException.class).when(userService).registerUser(any());
////
////    assertThrows(DuplicatedUserException.class, () -> {
////      String result = controller.processRegister(new User());
////    });
////  }
//}
//
//class TestPrincipal implements Principal {
//  private String name;
//
//  public TestPrincipal(String name) {
//    this.name = name;
//  }
//
//  @Override
//  public String getName() {
//    return name;
//  }
//
//  public void setName(String name) {
//    this.name = name;
//  }
//}

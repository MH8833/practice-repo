package com.project.SMS.controller;

import com.project.SMS.dto.EmailDetails;
import com.project.SMS.service.EmailServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmailServiceImplTest {

    @Mock
    private JavaMailSender javaMailSender;

    EmailServiceImpl emailService;

    @BeforeEach
    void setUp() {
        emailService = new EmailServiceImpl();
    }

    @Test
    void sendSimpleEmail_WithOneEmail(){
        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setSubject("test subject");
        emailDetails.setMsgBody("test body");
        emailDetails.setRecipients(List.of("elgatoska99@gmail.com"));
        emailDetails.setSender("smsapptesting@gmail.com");


        assertEquals("Mail Sent Successfully...", emailService.sendSimpleEmail(emailDetails));

    }


}

package com.project.SMS.config;

import com.project.SMS.UserManagement.*;
import com.project.SMS.model.*;
import com.project.SMS.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;




@Configuration
public class MockDataLoad {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private LessonFileRepository lessonFileRepository;
    @Autowired
    private LessonRepository lessonRepository;
    @Autowired
    private ContentBlockRepository contentBlockRepository;
    @Autowired
    private UserService userService;

    public boolean INSERT_COURSES = false;
    public boolean INSERT_USERS = false;
    public boolean INSERT_COURSE_CONTENTS = false;

    @Bean
    CommandLineRunner commandLineRunner(UserRepository userRepository){
        return args -> {

//            Course cc = courseRepository.getById(3L);
//            cc.getContentBlocks().forEach(contentBlock -> {
//                contentBlock.getLessons().forEach(lesson -> {
//
//                });
//            });


            if(INSERT_USERS) {

                if(roleRepository.count() == 0) {
                    Role roleAdmin = new Role("ADMIN");
                    Role roleStudent = new Role("STUDENT");
                    Role roleTeacher = new Role("TEACHER");
                    roleRepository.save(roleAdmin);
                    roleRepository.save(roleStudent);
                    roleRepository.save(roleTeacher);
                }

                Optional<Role> roleAdmin = roleRepository.findAdminRole();
                Optional<Role> roleTeacher = roleRepository.findTeacherRole();
                Optional<Role> roleStudent = roleRepository.findStudentRole();

                User userAdmin = new User("RICHARD", passwordEncoder.encode("adminpassword"),"richard", "velarde",
                        "velarde.a.ricardo@gmail.com", LocalDateTime.now(), LocalDate.now(), true, List.of(roleAdmin.get()));

                User userTeacher = new User("DANIEL", passwordEncoder.encode("userpassword"),"daniel", "velarde",
                        "velarde.a.ricardo@gmail.com", LocalDateTime.now(), LocalDate.now(), true, List.of(roleTeacher.get()));

                User user2 = new User("MARIANA", passwordEncoder.encode("userpassword"),"mariana", "boisson",
                        "elgatoska99@gmail.com", LocalDateTime.now(), LocalDate.now(), true, List.of(roleStudent.get()));

                User user3 = new User("JORGE", passwordEncoder.encode("userpassword"),"jorge", "alvarez",
                        "elgatoska99@gmail.com", LocalDateTime.now(), LocalDate.now(), true, List.of(roleStudent.get()));

                User user4 = new User("ANGEL", passwordEncoder.encode("userpassword"),"angel", "velarde",
                        "elgatoska99@gmail.com", LocalDateTime.now(), LocalDate.now(), true, List.of(roleStudent.get()));

                userRepository.save(userAdmin);
                userRepository.save(userTeacher);
                userRepository.save(user2);
                userRepository.save(user3);
                userRepository.save(user4);

                Teacher teacher = new Teacher("bio of a teacher", "Master", userTeacher);

                Student student1 = new Student(LocalDate.of(1992,3,5), user2);

                Student student2 = new Student(LocalDate.of(1990,11,27), user3);

                Student student3 = new Student(LocalDate.of(1987,4,16), user4);

                teacherRepository.save(teacher);
                studentRepository.save(student1);
                studentRepository.save(student2);
                studentRepository.save(student3);
            }

            if(INSERT_COURSES){

                User userTeacher = userService.getUserByUserName("DANIEL");
                User userStudent = userService.getUserByUserName("MARIANA");
                User userStudent1 = userService.getUserByUserName("JORGE");
                User userStudent2 = userService.getUserByUserName("ANGEL");

                Course course1 = new Course("C++ 0 TO HERO", "learn c++ from zero until you become a c++ hero",
                        CourseLevel.BEGINNER, LocalDate.now(), LocalDateTime.now(), 35.50, 1, 50,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent.getStudent())));

                Course course6 = new Course("JAVA 0 TO HERO", "learn JAVA from zero until you become a JAVA hero",
                        CourseLevel.INTERMEDIATE, LocalDate.now(), LocalDateTime.now(), 55.50, 1, 200,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent.getStudent())));

                Course course2 = new Course("C# 0 TO HERO", "learn c# from zero until you become a c# hero",
                        CourseLevel.EXPERT, LocalDate.now(), LocalDateTime.now(), 45.50, 1, 50,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent.getStudent())));

                Course course3 = new Course("Python 0 TO HERO", "learn Python from zero until you become a Python hero",
                        CourseLevel.ADVANCED, LocalDate.now(), LocalDateTime.now(), 35.50, 1, 30,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent.getStudent())));

                Course course4 = new Course("Kotlin 0 TO HERO", "learn Kotlin from zero until you become a Kotlin hero",
                        CourseLevel.BEGINNER, LocalDate.now(), LocalDateTime.now(), 35.0, 1, 20,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent.getStudent())) );

                Course course5 = new Course("JavaScript 0 TO HERO", "learn JavaScript from zero until you become a JavaScript hero",
                        CourseLevel.INTERMEDIATE, LocalDate.now(), LocalDateTime.now(), 35.50, 1, 44,
                        userTeacher.getTeacher(), new ArrayList<Student>());
                Course course7 = new Course("NEW COURSE FOR TEST", "learn JavaScript from zero until you become a JavaScript hero",
                        CourseLevel.INTERMEDIATE, LocalDate.now(), LocalDateTime.now(), 35.50, 0, 44,
                        userTeacher.getTeacher(), new ArrayList<Student>(Arrays.asList(userStudent1.getStudent())) );
                courseRepository.save(course7);
//                courseRepository.save(course1);
//                courseRepository.save(course2);
//                courseRepository.save(course3);
//                courseRepository.save(course4);
//                courseRepository.save(course5);
//                courseRepository.save(course6);

                LessonFile lessonFile = new LessonFile();
                lessonFile.setDataLink("https://sms-lessonfiles.s3.us-west-1.amazonaws.com/sample1.mp475ec3eae-4a90-40e0-bd50-4ea4f41c5bab");
                lessonFile.setFileExtension("mp4");
                lessonFile.setFileType("video/mp4");
                lessonFile.setFileName("sample1.mp475ec3eae-4a90-40e0-bd50-4ea4f41c5bab");

                lessonFileRepository.save(lessonFile);

                Lesson lesson = new Lesson();
                lesson.setLessonFile(lessonFile);
                lesson.setTitle("LESSON TITLE");
                lesson.setDescription("Lesson Description here...");

                ContentBlock contentBlock = new ContentBlock();
                contentBlock.setTitle("First week: Title");
                contentBlock.setDescription("in this week...");
                contentBlock.setCourse(course7);

                contentBlockRepository.save(contentBlock);
                lesson.setContentBlock(contentBlock);

                lessonRepository.save(lesson);

            }

            if(INSERT_COURSE_CONTENTS){
//                Course course = courseRepository.getById(23L);
//                LessonFile lessonFile1 = lessonFileRepository.getById(1L);
//                Lesson lesson = new Lesson();
//                lesson.setLessonFile(lessonFile1);
//                lesson.setTitle("LESSON TITLE");
//                lesson.setDescription("Lesson Description here...");
//
//
//                ContentBlock contentBlock = new ContentBlock();
//                contentBlock.setTitle("First week: Title");
//                contentBlock.setDescription("in this week...");
//                contentBlock.setCourse(course);
//
//                contentBlockRepository.save(contentBlock);
//                lesson.setContentBlock(contentBlock);
//
//                lessonRepository.save(lesson);

            }

        };

//    @Bean
//    CommandLineRunner commandLineRunner(UserRepository userRepository){
//        return args -> {
//            User user1 = new User("MARIANA","qwer", true, LocalDateTime.now(), LocalDate.now(), new Role("ADMIN"));
//            User user2 = new User("RICHARD","asdf", true, LocalDateTime.now(), LocalDate.now(), new Role("USER"));
////            Student dan = new Student("dan","esa@adf.com",44, LocalDate.of(1989, Month.APRIL,13), LocalDateTime.now());
////            Student ric = new Student("ric","asfsdfasdf@adf.com",32, LocalDate.of(1989, Month.APRIL,13), LocalDateTime.now());
//
//            userRepository.saveAll(List.of(user1,user2));
//        };

    }

}

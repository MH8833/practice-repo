package com.project.SMS.model;

import javax.persistence.*;

@Entity
@Table
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private String title;
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "LessonFile", nullable = false, referencedColumnName = "id")
    private LessonFile lessonFile;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", nullable = false, updatable = true)
    private ContentBlock contentBlock;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LessonFile getLessonFile() {
        return lessonFile;
    }

    public void setLessonFile(LessonFile lessonFile) {
        this.lessonFile = lessonFile;
    }

    public ContentBlock getContentBlock() {
        return contentBlock;
    }

    public void setContentBlock(ContentBlock contentBlock) {
        this.contentBlock = contentBlock;
    }
}

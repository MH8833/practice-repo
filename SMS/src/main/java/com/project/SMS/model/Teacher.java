package com.project.SMS.model;

import com.project.SMS.UserManagement.User;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String bio;
    private String title;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user", nullable = false, referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "teacher")
    private List<Course> courses;

    public Teacher(){}

    public Teacher(Long id, String bio, String title, User user) {
        this.id = id;
        this.bio = bio;
        this.title = title;
        this.user = user;
    }

    public Teacher(String bio, String title, User user) {
        this.bio = bio;
        this.title = title;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        Teacher teacher = (Teacher) o;
        return getId().equals(teacher.getId()) && getUser().equals(teacher.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser());
    }

}

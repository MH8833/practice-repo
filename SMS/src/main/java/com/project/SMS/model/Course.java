package com.project.SMS.model;

import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
//    @Column(columnDefinition = "VARCHAR(600)")
    @Column(length = 500, nullable = false)
    private String description;
    @Column(columnDefinition = "ENUM('BEGINNER', 'INTERMEDIATE', 'ADVANCED', 'EXPERT')")
    @Enumerated(EnumType.STRING)
    private CourseLevel level;
    private LocalDate publicationDate;
//    @DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm")
    private LocalDateTime startDate;
    @Column(nullable = false)
    private Double price;
    private Integer occupancy;
    private Integer occupancyLimit;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", nullable = false, updatable = true)
    private Teacher teacher;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "course_students",
            joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id")
    )
    private List<Student> students;

    @OneToMany(mappedBy = "course", orphanRemoval = true)
    private List<ContentBlock> contentBlocks;

    public List<ContentBlock> getContentBlocks() {
        return contentBlocks;
    }

    public void setContentBlocks(List<ContentBlock> contentBlocks) {
        this.contentBlocks = contentBlocks;
    }

    public Course(){
        contentBlocks = new ArrayList<>();
    };

    public Course(String name, String description, CourseLevel level, LocalDate publicationDate, LocalDateTime startDate,
                  Double price, Integer occupancy, Integer occupancyLimit, Teacher teacher, List<Student> students) {
        this.name = name;
        this.description = description;
        this.level = level;
        this.publicationDate = publicationDate;
        this.startDate = startDate;
        this.price = price;
        this.occupancy = students.size();
        if(occupancyLimit < this.occupancy)
            this.occupancyLimit = this.occupancy;
        else
            this.occupancyLimit = occupancyLimit;
        this.teacher = teacher;
        this.students = students;
    }

    public Course(Long id, String name, String description, CourseLevel level, LocalDate publicationDate,
                  LocalDateTime startDate, Double price, Integer occupancy, Integer occupancyLimit,
                  Teacher teacher, List<Student> students) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.level = level;
        this.publicationDate = publicationDate;
        this.startDate = startDate;
        this.price = price;
        this.occupancy = students.size();
        if(occupancyLimit < this.occupancy)
            this.occupancyLimit = this.occupancy;
        else
            this.occupancyLimit = occupancyLimit;
        this.teacher = teacher;
        this.students = students;
    }

    public void addNewContentBlock(){
        this.contentBlocks.add(new ContentBlock());
    }

    public void addContentBlock(ContentBlock contentBlock){
        this.contentBlocks.add(contentBlock);
    }

    public void addStudent(Student student){
        //TODO: check if needs more validations
        if(this.occupancyLimit != null && this.occupancy >= this.occupancyLimit){
            return;
        }
        if( students.contains(student))
            return;
        this.students.add(student);
        this.occupancy++;
    }

    public void removeStudent(Student student){
        if(students.remove(student)){
            this.occupancy -= 1;
        }
    }

    public void copy(Course course){
        this.id = course.getId() != null ? course.getId() : this.id ;
        this.name = course.getName() != null ? course.getName() : this.name ;
        this.description = course.getDescription() != null ? course.getDescription() : this.description ;
        this.level = course.getLevel() != null ? course.getLevel() : this.level ;
        this.publicationDate = course.getPublicationDate() != null ? course.getPublicationDate() : this.publicationDate;
        this.startDate = course.getStartDate() != null ? course.getStartDate() : this.startDate;
        this.price = course.getPrice() != null ? course.getPrice() : this.price ;
        this.occupancy = course.getOccupancy() != null ? course.getOccupancy() : this.occupancy ;
        this.occupancyLimit = course.getOccupancyLimit() != null ? course.getOccupancyLimit() : this.occupancyLimit;
        this.teacher = course.getTeacher() != null ? course.getTeacher() : this.teacher ;
        this.students = course.getStudents() != null ? course.getStudents() : this.students ;
    }

    public String toStringForEmail(){
        String courseString = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">" +
                "</head>\n" +
                "<body>" +
                "<h2><strong>" + this.name + "</strong></h2>\n" +
                "<p><strong>Description:</strong></p> <p>" + this.description +"</p>\n" +
                "<p><strong>level: </strong></p><p>"+ this.level +"</p>\n" +
                "<p><strong>start date: </strong></p><p>"+ this.startDate +"</p>\n" +
                "<p><strong>price: </strong></p><p>"+ this.price +"</p>" +
                "<p><strong>capacity: </strong></p><p>"+ this.occupancyLimit +"</p>"+
                "</body>"+
                "</html>";
        return courseString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CourseLevel getLevel() {
        return level;
    }

    public void setLevel(CourseLevel level) {
        this.level = level;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public void setStartDate(String startDate){
        DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
                .toFormatter();

//        LocalDateTime time = LocalDateTime.parse(startDate, formatter);
        LocalDateTime time = LocalDateTime.parse(startDate);
        this.startDate = time;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getOccupancy() {
//        return occupancy;
        return this.students.size();
    }

    public void setOccupancy(Integer occupancy) {
        this.occupancy = occupancy;
    }

    public Integer getOccupancyLimit() {
        return occupancyLimit;
    }

    public void setOccupancyLimit(Integer occupancyLimit) {
        this.occupancyLimit = occupancyLimit;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        if(this.teacher != null && this.teacher.equals(teacher))
            return;
        this.teacher = teacher;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
        this.occupancy = students.size();
    }





    //TODO: add safe methods for consistency in TEACHER, COURSE, STUDENT classes

}

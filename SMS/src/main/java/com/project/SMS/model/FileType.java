package com.project.SMS.model;

public enum FileType {
    VIDEO,
    IMAGE,
    AUDIO
}

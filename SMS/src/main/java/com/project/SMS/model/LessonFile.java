package com.project.SMS.model;

import javax.persistence.*;
import java.io.File;

@Entity
@Table
public class LessonFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String fileName;
    @Column(nullable = false)
    private String fileType;
    @Column(nullable = false)
//    @Enumerated(EnumType.STRING)
    private String fileExtension;
//    @Lob
//    private byte[] data;
    @Column(nullable = false)
    private String dataLink;

    public LessonFile(){}

    public LessonFile(String fileName, String fileType, String fileExtension, String dataLink) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileExtension = fileExtension;
        this.dataLink = dataLink;
    }

    public LessonFile(Long id, String fileName, String fileType, String fileExtension, String dataLink) {
        this.id = id;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileExtension = fileExtension;;
        this.dataLink = dataLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDataLink() {
        return dataLink;
    }

    public void setDataLink(String dataLink) {
        this.dataLink = dataLink;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }
}

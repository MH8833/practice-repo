package com.project.SMS.model;

import com.project.SMS.UserManagement.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Objects;

@Entity
@Table
public class Student {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer age;
    private LocalDate dateOfBirth;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user", nullable = false, referencedColumnName = "id")
    private User user;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name = "course_students",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id")
    )
    private List<Course> courses;

    public Student(){}

    public Student(LocalDate dateOfBirth, User user) {
//        this.age = LocalDate.now().getYear() - dateOfBirth.getYear();
        this.age = Period.between(dateOfBirth, LocalDate.now()).getYears();
        this.dateOfBirth = dateOfBirth;
        this.user = user;
    }

    public Student(Long id, Integer age, LocalDate dateOfBirth, User user) {
        this.id = id;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> students) {
        this.courses = students;
    }

    public void addCourse(Course course){
        if(this.courses.contains(course)){
            return;
        }
        this.courses.add(course);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getId().equals(student.getId()) && getUser().equals(student.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser());
    }
}

package com.project.SMS.model;

public enum FileExtension {
    MP4 (".mp4"),
    WMV (".wmv"),
    MOV (".mov"),
    FLV (".flv"),
    AVI (".avi"),
    MKV (".mkv"),
    JPEG (".jpeg"),
    JPG (".jpg"),
    PNG (".png"),
    PDF (".pdf"),
    TIFF (".tiff"),
    TIF (".tif"),
    BMP (".bmp");

    FileExtension(String s) {
    }

}

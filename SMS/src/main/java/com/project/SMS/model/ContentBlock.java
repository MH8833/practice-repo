package com.project.SMS.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class ContentBlock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    @OneToMany(mappedBy = "contentBlock")
    private List<Lesson> lessons;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id", updatable = true)
    private Course course;

    public ContentBlock(){
//        this.lessons = new ArrayList<>();
    }

    public ContentBlock(Long id, String title, String description, List<Lesson> lessons, Course course) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.lessons = lessons;
        this.course = course;
    }

    public ContentBlock(String title, String description, List<Lesson> lessons, Course course) {
        this.title = title;
        this.description = description;
        this.lessons = lessons;
        this.course = course;
    }

    public void addLesson(Lesson lesson){
        this.lessons.add(lesson);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}

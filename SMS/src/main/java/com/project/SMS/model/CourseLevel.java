package com.project.SMS.model;

public enum CourseLevel {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED,
    EXPERT
}

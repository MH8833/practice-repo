package com.project.SMS.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

//import com.amazonaws.regions.Region;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.paginators.ListObjectsV2Iterable;
import software.amazon.awssdk.services.s3.waiters.S3Waiter;

public class S3Utils {

    private static final String bucketName = "sms-lessonfiles";
    private static final String awsUrl = "https://sms-lessonfiles.s3.us-west-1.amazonaws.com/";

    public static void uploadFile(String fileName, InputStream inputStream)
            throws S3Exception, AwsServiceException, SdkClientException, IOException {
        S3Client client = S3Client.builder().build();

        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .acl("public-read")
                .contentType("video/mp4")
                .build();

        PutObjectResponse resp = client.putObject(request, RequestBody.fromInputStream(inputStream, inputStream.available()));
        System.out.println(resp.getClass());

        S3Waiter waiter = client.waiter();
        HeadObjectRequest waitRequest = HeadObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .build();

        WaiterResponse<HeadObjectResponse> waitResponse = waiter.waitUntilObjectExists(waitRequest);
        //IMPROVE: check if is necessary to wait
        waitResponse.matched().response().ifPresent(x -> {
            // run custom code that should be executed after the upload file exists

        });
    }

    public static List<String> getS3FilesListing(){

        S3Client client = S3Client.builder().region(Region.US_WEST_1).build();
        ListObjectsV2Request request = ListObjectsV2Request.builder().bucket("sms-lessonfiles").build();
        ListObjectsV2Iterable response = client.listObjectsV2Paginator(request);

        List<String> fileListing = new ArrayList<>();
        for (ListObjectsV2Response page : response) {
            page.contents().forEach((S3Object object) -> {
                System.out.println(object.key());
                fileListing.add(object.key());
            });
        }
        return fileListing;
    }

    public static void uploadFileTempDISABELED(String fileName, InputStream inputStream) throws IOException{
        Region region = Region.US_WEST_1;
        S3Client s3 = S3Client.builder()
                .region(region)
                .build();

//        createBucket(s3, bucketName, region);

        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key("sms-lessonfiles")
                .build();

        s3.putObject(objectRequest, RequestBody.fromInputStream(inputStream,inputStream.available()));
    }

    public static String getAwsUrl(){
        return awsUrl;
    }

    public static String getBucketName(){
        return bucketName;
    }
}
package com.project.SMS.dto;

import com.project.SMS.model.Course;
import com.project.SMS.model.CourseLevel;
import com.project.SMS.model.Teacher;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CourseDto {

    private String name;
    private String description;
    private CourseLevel courseLevel;
    private LocalDate publicationDate;
    private LocalDateTime startDate;
    private Double price;
    private Integer occupancy;
    private Integer occupancyLimit;
    private String teacherUserName;
    private List<String> studentsUserNames;

    public CourseDto(Course course){
        this.name = course.getName();
        this.description = course.getDescription();
        this.courseLevel = course.getLevel();
        this.publicationDate = course.getPublicationDate();
        this.startDate = course.getStartDate();
        this.price = course.getPrice();
        this.occupancy = course.getOccupancy();
        this.occupancyLimit = course.getOccupancyLimit();
        this.teacherUserName = course.getTeacher().getUser().getUserName();
        this.studentsUserNames = new ArrayList<String>();
        course.getStudents().forEach((student -> {
            studentsUserNames.add(student.getUser().getUserName());
        }));
    }

}

package com.project.SMS.dto;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Student;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class StudentDto extends UserDto{

    private Integer age;
    private LocalDate dateOfBirth;
    private List<CourseDto> courses;

    StudentDto(){}

    StudentDto(Student student){
        super(student.getUser());
        this.age = student.getAge();
        this.dateOfBirth = student.getDateOfBirth();
        this.courses = new ArrayList<CourseDto>();
        student.getCourses().forEach((course -> {
            courses.add(new CourseDto(course));
        }));
    }

}

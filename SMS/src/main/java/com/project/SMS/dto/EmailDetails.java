package com.project.SMS.dto;


import lombok.Data;
import org.apache.logging.log4j.util.Strings;

import java.io.Serializable;
import java.util.*;


public class EmailDetails implements Serializable {

    private List<String> recipients;
    private String msgBody;
    private String subject;
    private String attachment;
    private String sender;

    public EmailDetails() {
//        this.recipients = Collections.EMPTY_LIST;
//        this.msgBody = "";
//        this.subject = "";
//        this.attachment = "";
//        this.sender = "";
    }

    public EmailDetails(List<String> recipients, String msgBody, String subject, String attachment, String sender) {
        this.recipients = recipients;
        this.msgBody = msgBody;
        this.subject = subject;
        this.attachment = attachment;
        this.sender = sender;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void addRecipient(String recipient){

        if(Strings.isEmpty(recipient)){
            return;
        }
        if(this.recipients == null){
            //IMPROVE how to initialize a list
            this.recipients = new ArrayList<String>(Arrays.asList(recipient));
            return;
        }
        this.recipients.add(recipient);
    }
}

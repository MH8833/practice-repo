package com.project.SMS.dto;

public enum Roles {
    ADMIN,
    STUDENT,
    TEACHER
}

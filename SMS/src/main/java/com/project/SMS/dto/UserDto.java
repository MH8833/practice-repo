package com.project.SMS.dto;

import com.project.SMS.UserManagement.User;
import lombok.Data;


@Data
public class UserDto {

    private String userName;
    private String firstName;
    private String lastName;
    private String email;

    UserDto(){}

    UserDto(User user){
        this.userName = user.getUserName();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
    }

}

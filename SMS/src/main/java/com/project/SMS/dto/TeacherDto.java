package com.project.SMS.dto;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Teacher;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TeacherDto extends UserDto {

    private String bio;
    private String title;
    private List<CourseDto> courses;

    TeacherDto(){}

    TeacherDto(Teacher teacher){
        super(teacher.getUser());

        this.bio = teacher.getBio();
        this.title = teacher.getTitle();

        this.courses = new ArrayList<CourseDto>();
        teacher.getCourses().forEach((course -> {
            courses.add(new CourseDto(course));
        }));
    }

}

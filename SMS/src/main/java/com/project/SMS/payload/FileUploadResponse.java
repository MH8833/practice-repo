package com.project.SMS.payload;

import com.project.SMS.model.LessonFile;

public class FileUploadResponse {

    private String filename;
    private String fileDownloadUri;
    private String fileType;
    private LessonFile lessonFile;
    private Long size;

    public FileUploadResponse(String filename, String fileDownloadUri, String fileType, LessonFile lessonFile, Long size) {
        this.filename = filename;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.lessonFile = lessonFile;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public LessonFile getLessonFile() {
        return lessonFile;
    }

    public void setLessonFile(LessonFile lessonFile) {
        this.lessonFile = lessonFile;
    }

}

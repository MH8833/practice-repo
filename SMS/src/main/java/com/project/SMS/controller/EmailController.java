package com.project.SMS.controller;

import com.project.SMS.model.Course;
import com.project.SMS.dto.EmailDetails;
import com.project.SMS.model.Student;
import com.project.SMS.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EmailController {

    @Autowired
    private final EmailService emailService;

    EmailController(EmailService emailService) {
        this.emailService = emailService;
    }

    public String sendMailWithAttachment(EmailDetails emailDetails) {
        return emailService.sendEmail(emailDetails);
    }

    public String sendSimpleEmail(EmailDetails emailDetails){
        return emailService.sendSimpleEmail(emailDetails);
    }

    public String sendEmail(EmailDetails emailDetails){
        return emailService.sendEmail(emailDetails);
    }

    //TODO: check if useful to set sender as parameter
    public String sendEmailToStudentsInCourse(Course course, String subject, String msgBody){

        List<Student> students = course.getStudents();
        if(students.isEmpty()){
            return "Students not found in this course";
        }
        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setMsgBody(msgBody);
        emailDetails.setSubject(subject);

        for(Student student : students){
            emailDetails.addRecipient(student.getUser().getEmail());
        }

        return emailService.sendEmail(emailDetails);
    }

}

package com.project.SMS.controller;

import com.project.SMS.UserManagement.Role;
import com.project.SMS.UserManagement.RoleRepository;
import com.project.SMS.model.LessonFile;
import com.project.SMS.payload.FileUploadResponse;
import com.project.SMS.service.FileStorageService;
import com.project.SMS.utils.S3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOError;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@RestController
@RequestMapping(path = "/file")
public class FileController {

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private RoleRepository roleRepository;

//    @PostMapping(path="/uploadFile")
//    public FileUploadResponse uploadFile(@RequestParam(name = "file") MultipartFile file) {
//        LessonFile dbFile = fileStorageService.storeFile(file);
//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(dbFile.getId().toString())
//                .toUriString();
//
//        return new FileUploadResponse(dbFile.getFileName(), fileDownloadUri,
//                file.getContentType(), file.getSize());
//    }

    @PostMapping(path="/uploadFile/S3")
    public FileUploadResponse uploadLessonFile(@RequestParam(name = "file") MultipartFile file, Authentication auth) {

        if(auth == null || !auth.isAuthenticated()){
            return new FileUploadResponse("not logged. error","","", new LessonFile(), 0L);
        }
        if( !(authIsAdmin(auth) || authIsTeacher(auth)) ){
            return new FileUploadResponse("don't have the credentials. error","","",new LessonFile(), 0L);
        }

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())) + UUID.randomUUID();
        try{
            S3Utils.uploadFile(fileName,file.getInputStream());
        }catch (IOException e){
            System.out.println(e.getMessage());
        }


        LessonFile dbFile = fileStorageService.storeFile(file, fileName);

//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(dbFile.getId().toString())
//                .toUriString();
        String fileDownloadUri = ServletUriComponentsBuilder.fromPath(dbFile.getDataLink()).toUriString();

        return new FileUploadResponse(dbFile.getFileName(), fileDownloadUri, file.getContentType(),new LessonFile(), file.getSize());
    }

    private boolean authIsTeacher(Authentication auth){
        Role teacherRole = roleRepository.findTeacherRole().get();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(teacherRole.getRoleName());
        return auth.getAuthorities().contains(grantedAuthority);
    }

    private boolean authIsAdmin(Authentication auth){
        Role adminRole = roleRepository.findAdminRole().get();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(adminRole.getRoleName());
        return auth.getAuthorities().contains(grantedAuthority);
    }




//    @GetMapping("/downloadFile/{fileId}")
//    public ResponseEntity<Resource> downloadFile (@PathVariable Long fileId){
//        LessonFile dbFile = fileStorageService.getFile(fileId);
//        ResourceLoader loadder;
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "inline;")
//                .body(new ByteArrayResource(dbFile.getData()));
//    }

}

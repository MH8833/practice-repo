package com.project.SMS.controller;

//IMPROVE: review empty constructors to set  default values
//IMPROVE: for strigs check Strings.isBlank() / Strings.isEmpty


import com.project.SMS.UserManagement.*;
import com.project.SMS.dto.EmailDetails;
import com.project.SMS.dto.Roles;
import com.project.SMS.model.*;
import com.project.SMS.repository.ContentBlockRepository;
import com.project.SMS.repository.LessonFileRepository;
import com.project.SMS.repository.LessonRepository;
import com.project.SMS.scheduler.SchedulerController;
import com.project.SMS.service.*;
import com.project.SMS.exception.DuplicatedUserException;
import com.project.SMS.utils.S3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.EnumMap;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/")
public class SMS_Controller{

    @Autowired
    private final UserService userService;

    @Autowired
    private final CourseService courseService;

    @Autowired
    private final StudentService studentService;

    @Autowired
    private final TeacherService teacherService;

    @Autowired
    private final RoleRepository roleRepository;

    @Autowired
    private final EmailController emailController;

    @Autowired
    private SchedulerController schedulerController;

    @Autowired
    private ContentBlockService contentBlockService;

    @Autowired
    private FileManagementController fileManagementController;

    /////////////////////////////
    @Autowired
    private LessonRepository lessonRepository;

    @Autowired
    private LessonFileRepository lessonFileRepository;

    @Autowired
    private ContentBlockRepository contentBlockRepository;
////////////////////////////////////////

    public SMS_Controller(UserService userService, CourseService courseService,
                          StudentService studentService, TeacherService teacherService,
                          RoleRepository roleRepository, EmailController emailController){
        this.userService = userService;
        this.courseService = courseService;
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.roleRepository = roleRepository;
        this.emailController = emailController;
    }

    @GetMapping
    public ModelAndView home(Principal principal, Authentication auth){

        List<Course> courses = courseService.getAllCourses();

        /////////////////////4
//        Course course = courseService.getById(23L);
//        LessonFile lessonFile1 = lessonFileRepository.getById(1L);
//        Lesson lesson = lessonRepository.getById(3L);
//        lesson.setLessonFile(lessonFile1);
//        lesson.setTitle("LESSON TITLE");
//        lesson.setDescription("Lesson Description here...");
//
//        ContentBlock contentBlock = new ContentBlock();
//        contentBlock.setTitle("First week: Title");
//        contentBlock.setDescription("in this week...");
//        contentBlock.setCourse(course);
//
//        lesson.setContentBlock(contentBlock);
//
//        contentBlockRepository.save(contentBlock);
//        lessonRepository.save(lesson);

        /////////////////////////////

        ModelAndView homeView = new ModelAndView("home");
        homeView.addObject("principal", principal);
        homeView.addObject("courses", courses);

        if(auth != null){
            User user = userService.getUserByUserName(auth.getName());
            homeView.addObject(getUserType(auth), user);
        }

        return homeView;
    }

    @GetMapping("/login")
    public ModelAndView userLogin(){
        return new ModelAndView("login");
    }

    @GetMapping("/register")
    public ModelAndView userRegister(@RequestParam(name="error", required = false) String errorMessage ){
//        request.getParameterMap().forEach((k,v)->{
//            System.out.println(k+" "+v);
//        });
//        redirectAttrs.getFlashAttributes().forEach((k,v)->{
//            System.out.println(k+" "+v);
//        });
        System.out.println(errorMessage);

        return new ModelAndView("register").addObject("user", new User());
    }

    @PostMapping("/register")
    public ModelAndView processRegister(User user) {
        ModelAndView model = new ModelAndView();

        try {
            userService.registerUser(user);
        } catch (DuplicatedUserException e) {
//            redirectAttrs.addFlashAttribute("errorMessage", e.getMessage());
            return new ModelAndView("redirect:/register?error");
        }

        return new ModelAndView("redirect:/");
    }

    @GetMapping("/userProfile/{username}")
    public ModelAndView userProfile(@PathVariable String username, Principal principal, Authentication auth){
//TODO: check ModelAndView exacly how it works and other alternatives
        if(auth == null || !auth.isAuthenticated()){
            return new ModelAndView("redirect:/login");
        }
        //improve?: review this implementation
        User user = new User();
        try {
            user = userService.getUserByUserName(username);

            if( !user.getUserName().equals(auth.getName())){
                return new ModelAndView("redirect:/userProfile/"+auth.getName());
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        ModelAndView model = new ModelAndView("userProfile");
        model.addObject("user",user);
        model.addObject("principal", principal);

//        if(user.getRoles().contains(roleRepository.findStudentRole().get())){
        if(user.isStudent()){
            model.addObject("courses", user.getStudent().getCourses());
        }

        return model;
    }

    @GetMapping("/courses/{courseId}")
    public ModelAndView courseDetails(@PathVariable Long courseId, Principal principal, Authentication auth){
        //TODO: ADD ALL VALIDATIONS (at all levevles) THIS IS A TEMPORAL IMPLEMENTATION POV
        if( auth == null || !auth.isAuthenticated()){
            return new ModelAndView("redirect:/login");
        }

        ModelAndView courseView = new ModelAndView("courseDetails");

        try{
            Course course = courseService.getById(courseId);
            User user = userService.getUserByUserName(auth.getName());

            if( (authIsTeacher(auth) && course.getTeacher().equals(user.getTeacher()) ) || authIsAdmin(auth) )  {
                courseView.addObject("canEdit", true);
                courseView.addObject("canDelete", true);
            }else if( authIsStudent(auth) && !course.getStudents().contains(user.getStudent())){
                courseView.addObject("canEnroll", true);
            }else{
                courseView.addObject("canUnenroll", true);
            }
            courseView.addObject("course", course);
            courseView.addObject("contentBlocks", course.getContentBlocks());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        courseView.addObject("principal", principal);

        return courseView;
    }

    @GetMapping("/courses/edit/{courseId}")
    public ModelAndView editCourse(@PathVariable Long courseId, Principal principal, Authentication auth){

        if(auth == null || !auth.isAuthenticated()){
            return new ModelAndView("redirect:/login");
        }

        ModelAndView modelCourseEdit = new ModelAndView("editCourse");

        try{
            Course course = courseService.getById(courseId);

            modelCourseEdit.addObject("course", course);
            modelCourseEdit.addObject("principal", principal);
            modelCourseEdit.addObject("courseLevels", CourseLevel.values());
            modelCourseEdit.addObject("contentBlocks", course.getContentBlocks());

            return modelCourseEdit;

        }catch (NoSuchElementException e){
            System.out.println(e.getMessage());
//            return new ModelAndView("redirect:/courses/edit"+courseId);
            return new ModelAndView("redirect:/");
        }
    }

    @PostMapping("/courses/edit/save/{courseId}")
    public ModelAndView saveEditedCourse(@ModelAttribute Course course, Authentication auth,
                                         @PathVariable Long courseId, @RequestParam(name="start_date") String startDate,
                                         @RequestParam(name="notifyStudents",required=false) String notifyStudents){

        if(!auth.isAuthenticated()){
            return new ModelAndView("redirect:/login");
//            return new ModelAndView("redirect:/courses/edit/"+course.getId()).addObject(course);
        }
        try{
//IMPROVE::     NOT REALLY JUST CHECK THIS, teacher is null  since the object course is not complete( FIX IT)
//            if(!auth.getName().equals(course.getTeacher().getUser().getUserName())){
//                return new ModelAndView("redirect:/courses/edit/"+course.getId()).addObject(course);
//            }
            LocalDateTime dateTime = LocalDateTime.parse(startDate);
            boolean startDateChanged = !courseService.getById(courseId).getStartDate().equals(dateTime);

            course.setStartDate(dateTime);
            Course dbCourse = courseService.updateCourse(course, courseId);

            if( startDateChanged )
                schedulerController.updateEmailRemainderToStudentsInCourse(dbCourse);

            if( notifyStudents != null )
                emailController.sendEmailToStudentsInCourse(dbCourse, "Course has been edited", dbCourse.toStringForEmail());

        }catch (RuntimeException e /*NoSuchElementException e*/){
            System.out.println(e.getMessage());
        }

        return new ModelAndView("redirect:/");
    }
    @PostMapping("/deleteCourse")
    public void deleteCourse(@RequestParam(value = "courseId") Long courseId,
                             HttpServletResponse response,
                             Authentication auth){
        try{
            Course course = courseService.getById(courseId);
            if(course.getOccupancy() > 0){
                //IMPROVE: set restictions when the course have users?
                String subject = "The course: "+ course.getName() + " has been deleted";
                emailController.sendEmailToStudentsInCourse(course, subject, course.toStringForEmail());
            }

            courseService.deleteCourse(courseId);
            response.sendRedirect("/");
        }catch (NoSuchElementException | IOException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @PostMapping("/enrollToCourse")
    public void registerToCourse(@RequestParam(value = "courseId") Long courseId,
                                 HttpServletResponse response,
                                 Authentication auth){

        try{
            User user = userService.getUserByUserName(auth.getName());

            Student student = studentService.getStudentByUser(user);
            Course course = courseService.getById(courseId);

            courseService.enrollStudentToCourse(course, student);

            response.sendRedirect("/userProfile/"+auth.getName());

        }catch (NoSuchElementException | IOException exception) {
            System.out.println(exception.getMessage());
        }

    }

    @PostMapping(path="/unenrollFromCourse")
    public void unenrollFromCourse(@RequestParam(name = "courseId") Long courseId, Authentication auth,
                                   HttpServletResponse response){

        try{
            User user = userService.getUserByUserName(auth.getName());
            Student student = studentService.getStudentByUser(user);
            Course course = courseService.getById(courseId);

            courseService.unenrollStudentToCourse(course, student);

            response.sendRedirect("/userProfile/"+auth.getName());
        }catch (NoSuchElementException | IOException exception){
            System.out.println(exception.getMessage());
        }
    }

    @GetMapping(path = "/addCourse")
    public ModelAndView addNewCourse(Principal principal){

        ModelAndView model = new ModelAndView("addCourse");
        model.addObject("course", new Course());
        model.addObject("principal", principal);
        model.addObject("courseLevels", CourseLevel.values());

        return model;
    }

    @PostMapping(path = "/addCourse")
    public void addNewCourse(@ModelAttribute("course") Course course,
                             @RequestParam(value = "teacherName") String teacherName,
                             @RequestParam(value = "start_date") String startDate,
                             HttpServletResponse response, Model model,
                             Principal principal, Authentication auth){
//Improve: set teacher name optional only for admin, for teacher get from auth
        try{
            User user = userService.getUserByUserName(teacherName);
            Teacher teacher = teacherService.findTeacherByUser(user);

            course.setTeacher(teacher);
//            course.setStartDate(startDate);
            course.setStartDate(startDate);
            course.setPublicationDate(LocalDate.now());
            courseService.saveNewCourse(course);

//            CourseDto courseDto = new CourseDto(course);
            schedulerController.scheduleEmailRemainderToStudentsInCourse(course);

            response.sendRedirect("/");
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    @GetMapping("/getContentBlocks/{courseId}")
    public ModelAndView getContentBlocks(@PathVariable Long courseId){

        ModelAndView model = new ModelAndView("addCourseContent :: newContentBlock");
        try{
            Course course = courseService.getById(courseId);
            course.addNewContentBlock();
            model.addObject("contentBlocks", course.getContentBlocks());
            model.addObject("courseId", course.getId());

        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        return model;
    }

    @GetMapping("/newContentBlock/{courseId}")
    public ModelAndView getNewContentBlock(@PathVariable Long courseId){

        ModelAndView model = new ModelAndView("addCourseContent :: newContentBlockForm");
        try{
            Course course = courseService.getById(courseId);
            course.addNewContentBlock();
            model.addObject("contentBlock", course.getContentBlocks().get(course.getContentBlocks().size()-1));
            model.addObject("courseId", course.getId());

        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        return model;
    }

    @PostMapping("/save/newContentBlock/{courseId}")
    public void saveNewContentBlock(@PathVariable Long courseId,
                                    @RequestParam(name="title") String title,
                                    @RequestParam(name = "description") String description){
        try{
            Course course = courseService.getById(courseId);
            ContentBlock contentBlock = new ContentBlock();
            contentBlock.setDescription(description);
            contentBlock.setTitle(title);
            contentBlock.setCourse(courseService.getById(courseId));
            contentBlockRepository.save(contentBlock);
            course.addContentBlock(contentBlock);
//            course.addNewContentBlock(editedCourse.getContentBlocks().get(editedCourse.getContentBlocks().size()-1));
//            course.setContentBlocks(editedCourse.getContentBlocks());

            courseService.updateCourse(course);

        }catch (RuntimeException e){
            System.out.println(e.getMessage());
        }
    }

    @GetMapping("/newLesson/{blockId}")
    public ModelAndView createNewLesson(@PathVariable Long blockId){

        ModelAndView model = new ModelAndView("addCourseContent :: createNewLesson");
        try{
            Lesson lesson = new Lesson();
            model.addObject("lesson", lesson);
            model.addObject("blockId", blockId);

        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        return model;
    }

    @PostMapping("/saveNewLesson/{blockId}")
    public void saveNewLesson(@PathVariable Long blockId,
                              @RequestParam(name = "title") String title,
                              @RequestParam(name = "description") String description,
                              @RequestParam(name = "file") MultipartFile file){

//        Course course = courseService.getById(23L);
//        ContentBlock contentBlock2 = contentBlockService.getByCourseId(course, blockId);
//
//        ContentBlock contentBlock = contentBlockRepository.getById(blockId);

        ContentBlock contentBlock = contentBlockService.getById(blockId);


        Lesson lesson = new Lesson();
        lesson.setContentBlock(contentBlock);
        lesson.setTitle(title);
        lesson.setDescription(description);


        LessonFile lessonFile = fileManagementController.uploadLessonFile(file);

        lesson.setLessonFile(lessonFile);
        lessonRepository.save(lesson);

    }



    private boolean authIsStudent(Authentication auth){
        Role studentRole = roleRepository.findStudentRole().get();
        SimpleGrantedAuthority studentAuthority = new SimpleGrantedAuthority(studentRole.getRoleName());
        return auth.getAuthorities().contains(studentAuthority);
    }

    private boolean authIsTeacher(Authentication auth){
        Role teacherRole = roleRepository.findTeacherRole().get();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(teacherRole.getRoleName());
        return auth.getAuthorities().contains(grantedAuthority);
    }

    private boolean authIsAdmin(Authentication auth){
        Role adminRole = roleRepository.findAdminRole().get();
        SimpleGrantedAuthority grantedAuthority = new SimpleGrantedAuthority(adminRole.getRoleName());
        return auth.getAuthorities().contains(grantedAuthority);
    }

    private String getUserType(Authentication auth){
        String userType = "";
        if(authIsStudent(auth)){
            userType = Roles.STUDENT.toString();
        }else if(authIsTeacher(auth)){
            userType = Roles.TEACHER.toString();
        }else if(authIsAdmin(auth)){
            userType = Roles.ADMIN.toString();
        }
        return userType;
    }

}


package com.project.SMS.controller;

import com.project.SMS.UserManagement.Role;
import com.project.SMS.UserManagement.RoleRepository;
import com.project.SMS.model.LessonFile;
import com.project.SMS.payload.FileUploadResponse;
import com.project.SMS.service.FileStorageService;
import com.project.SMS.utils.S3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.UUID;

@Component
public class FileManagementController {

    @Autowired
    private FileStorageService fileStorageService;

    public FileManagementController(FileStorageService fileStorageService){
        this.fileStorageService = fileStorageService;
    }

    public LessonFile uploadLessonFile(MultipartFile file) {

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename())) + UUID.randomUUID();
        try{
            S3Utils.uploadFile(fileName,file.getInputStream());
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        LessonFile dbFile = fileStorageService.storeFile(file, fileName);

        String fileDownloadUri = ServletUriComponentsBuilder.fromPath(dbFile.getDataLink()).toUriString();

        return dbFile;
    }


//    @GetMapping("/downloadFile/{fileId}")
//    public ResponseEntity<Resource> downloadFile (@PathVariable Long fileId){
//        LessonFile dbFile = fileStorageService.getFile(fileId);
//        ResourceLoader loadder;
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "inline;")
//                .body(new ByteArrayResource(dbFile.getData()));
//    }

}

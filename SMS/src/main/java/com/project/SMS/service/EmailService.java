package com.project.SMS.service;

import com.project.SMS.dto.EmailDetails;

public interface EmailService {

    String sendSimpleEmail(EmailDetails emailDetails);

    String sendEmail(EmailDetails emailDetails);


}

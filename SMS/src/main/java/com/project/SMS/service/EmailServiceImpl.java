package com.project.SMS.service;

//import com.project.SMS.config.EmailConfig;
import com.project.SMS.dto.EmailDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.core.io.FileSystemResource;
import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
//@Component
public class EmailServiceImpl implements EmailService{

    @Autowired
    private JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String sender;

//    public EmailServiceImpl(){
//        this.javaMailSender = new EmailConfig().getJavaMailSender();
//    }

    @Override
    public String sendSimpleEmail(EmailDetails emailDetails) {

        try {
            SimpleMailMessage mailMessage
                    = new SimpleMailMessage();

            if(emailDetails.getSender() == null || emailDetails.getSender().isEmpty()){
                mailMessage.setFrom(sender);
            }else{
                mailMessage.setFrom(emailDetails.getSender());
            }

            if(emailDetails.getRecipients().isEmpty()) {
                throw new Exception();
            }
            mailMessage.setTo(emailDetails.getRecipients().get(0));
            mailMessage.setText(emailDetails.getMsgBody());
            mailMessage.setSubject(emailDetails.getSubject());

            javaMailSender.send(mailMessage);
            return "Mail Sent Successfully...";

        } catch (Exception e) {
            return "Error while Sending Mail";
        }

    }

    @Override
    public String sendEmail(EmailDetails emailDetails) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper;

        try {

            mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            if(emailDetails.getSender() == null || emailDetails.getSender().isEmpty())
                mimeMessageHelper.setFrom(sender);
            else
                mimeMessageHelper.setFrom(emailDetails.getSender());

            emailDetails.getRecipients().forEach((rec)->{
                try {
                    mimeMessageHelper.addTo(rec);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            });

            mimeMessageHelper.setText(emailDetails.getMsgBody());
            mimeMessage.setContent(emailDetails.getMsgBody(),"text/html");
            mimeMessageHelper.setSubject(emailDetails.getSubject());

            if(emailDetails.getAttachment() != null && !emailDetails.getAttachment().isEmpty()) {
                FileSystemResource file = new FileSystemResource(new File(emailDetails.getAttachment()));

                if (file.getFilename() != null)
                    mimeMessageHelper.addAttachment(file.getFilename(), file);
            }

            javaMailSender.send(mimeMessage);
            return "Mail sent Successfully";
        }catch (MessagingException | MailException e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }
}

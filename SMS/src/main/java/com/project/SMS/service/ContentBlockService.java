package com.project.SMS.service;

import com.project.SMS.model.ContentBlock;
import com.project.SMS.model.Course;
import com.project.SMS.repository.ContentBlockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class ContentBlockService {

    @Autowired
    private ContentBlockRepository contentBlockRepository;

    public ContentBlock getById(Long id){
        return contentBlockRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Content Block with id: "+id+" not found"));

    }

    public ContentBlock getByCourseId(Course course, Long blockId){
        return contentBlockRepository.getByCourseId(course, blockId)
                .orElseThrow(() -> new NoSuchElementException("Content Block with id: "+course.getId()+" not found"));

    }

}

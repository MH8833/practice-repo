package com.project.SMS.service;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Student;
import com.project.SMS.repository.StudentRepository;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    public Student getStudentByUser(User user){
        return studentRepository.findByStudentByUser(user)
                .orElseThrow(()->new NoSuchElementException("Student with username: "+user.getUserName()+" not found"));
    }
}

package com.project.SMS.service;

import com.project.SMS.exception.FileStorageException;
import com.project.SMS.model.LessonFile;
import com.project.SMS.repository.LessonFileRepository;
import com.project.SMS.utils.S3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@Service
public class FileStorageService {

    @Autowired
    private final LessonFileRepository lessonFileRepository;

    @Autowired
    public FileStorageService(LessonFileRepository lessonFileRepository){
        this.lessonFileRepository = lessonFileRepository;
    }

    public LessonFile storeFile(MultipartFile file, String fileName) {
        // Normalize file name
        if(fileName.isBlank()){
            fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        }

        String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());

//TODO: check how to implement more unique names/s3 file name

        // Check if the file's name contains invalid characters

        if(fileName.contains("..")) {
            throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
        }
        String awsLink = S3Utils.getAwsUrl() + fileName;
        LessonFile dbFile = new LessonFile(fileName, file.getContentType(), fileExtension, awsLink);
        return lessonFileRepository.save(dbFile);

    }

    public LessonFile getFile(Long fileId){

        return lessonFileRepository.findById(fileId)
                .orElseThrow(() -> new FileStorageException("LessonFile not found with id " + fileId));
    }

}

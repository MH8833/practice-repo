package com.project.SMS.service;

import com.project.SMS.dto.CourseDto;
import com.project.SMS.model.Course;
import com.project.SMS.model.Student;
import com.project.SMS.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CourseService {

    @Autowired
    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getAllCourses(){
        return courseRepository.findAll();
    }

    public Course getById(Long id){
        return courseRepository.findById(id)
                    .orElseThrow(()->new NoSuchElementException("Course with id: "+ id+ " not found"));
    }

//    public Course getById(Long id){
//        return courseRepository.findById(id).orElseThrow(()->new NoSuchElementException("Course not found"));
//    }

    public boolean enrollStudentToCourse(Course course, Student student){

        if(course.getOccupancyLimit() != null && course.getOccupancy() >= course.getOccupancyLimit()){
            return false;
        }

        course.addStudent(student);
        courseRepository.save(course);

        return true;
    }

    public void unenrollStudentToCourse(Course course, Student student){
        course.removeStudent(student);
        courseRepository.save(course);
    }

    public void saveNewCourse(Course course){
        course.setOccupancy(0);
        if(course.getOccupancyLimit() == null || course.getOccupancyLimit() <= 0){
            course.setOccupancyLimit(50); //50 default limit
        }
        course.setPublicationDate(LocalDate.now());

        courseRepository.save(course);
    }

    public void updateCourse(Course course) {
        if( course.getId() == null || !courseRepository.existsById(course.getId())){
            throw new IllegalStateException("course does not exist");
        }

        courseRepository.save(course);
    }

     public Course updateCourse(Course course, Long courseId) {
        if( !courseRepository.existsById(courseId)){
            throw new IllegalStateException("course with id: "+ courseId + "does not exist");
        }
        Course dbCourse = this.getById(courseId);

        dbCourse.copy(course);

        courseRepository.save(dbCourse);

        return dbCourse;
    }

    public void deleteCourse(Long courseId){
        Course course = courseRepository.getById(courseId);
        courseRepository.delete(course);
    }

    public void deleteCourse(Course course){
        courseRepository.delete(course);
    }


}

package com.project.SMS.service;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Teacher;
import com.project.SMS.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TeacherService {

    @Autowired
    private final TeacherRepository teacherRepository;

    public TeacherService(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    public List<Teacher> getAllTeachers(){
        return teacherRepository.findAll();
    }

//    public Optional<Teacher> findTeacherByUser(User user){
//        return teacherRepository.findByTeacherByUser(user);
//    }
    public Teacher findTeacherByUser(User user){
        return teacherRepository.findByTeacherByUser(user)
                .orElseThrow(() -> new NoSuchElementException("Teacher with username "+user.getUserName()+" not found"));
    }

//    public List<Teacher> getTeacherByUserName(String username){
//
//    }
}

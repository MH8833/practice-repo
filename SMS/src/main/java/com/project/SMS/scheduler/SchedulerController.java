package com.project.SMS.scheduler;

import com.project.SMS.dto.EmailDetails;
import com.project.SMS.model.Course;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class SchedulerController {

    @Autowired
    private Scheduler scheduler;

    //Improve: review if could be useful latter
    public void scheduleSingleEmail(SchedulerEmailData schedulerEmailData){

        try{
            if(schedulerEmailData.getTimeZone() == null){
                schedulerEmailData.setTimeZone(ZoneId.systemDefault());
            }
            ZonedDateTime dateTime = ZonedDateTime.of(schedulerEmailData.getDateTime(), schedulerEmailData.getTimeZone());
            JobDetail jobDetail = buildJobDetail(schedulerEmailData.getEmailDetails());
            Trigger trigger = buildJobTrigger(jobDetail, dateTime);
            scheduler.scheduleJob(jobDetail, trigger);

        }catch (SchedulerException exception){
            System.out.println(exception.getMessage());
        }
    }
    //Improve: review if could be useful latter
    public void scheduleMultipleEmails(List<SchedulerEmailData> emailJobDetails){
        emailJobDetails.forEach(emailDetails -> {
            try{
                if(emailDetails.getTimeZone() == null){
                    emailDetails.setTimeZone(ZoneId.systemDefault());
                }
                ZonedDateTime dateTime = ZonedDateTime.of(emailDetails.getDateTime(), emailDetails.getTimeZone());
                JobDetail jobDetail = buildJobDetail(emailDetails.getEmailDetails());
                Trigger trigger = buildJobTrigger(jobDetail, dateTime);
                scheduler.scheduleJob(jobDetail, trigger);



            }catch (SchedulerException exception){
                System.out.println(exception.getMessage());
            }
        });
    }

    public void scheduleEmailRemainderToStudentsInCourse(Course course){

        try{
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("courseId", course.getId());
            String jobId = course.getId() + "courseRemainder";
            String jobGroup = "email-remainder-jobs";
            String jobDescription = "Send email remainder to all students in course Job";
            JobDetail jobDetail = buildJobDetail(jobDataMap, jobId, jobGroup, jobDescription);

            String triggerGroup = "email-reminder-triggers";
            String triggerDescription = "Send email remainder to all students in course Trigger";
            Trigger trigger = buildJobTrigger(jobDetail, course.getStartDate().minusDays(1),
                                                triggerGroup, triggerDescription);

            scheduler.scheduleJob(jobDetail, trigger);

//            for (String groupName : scheduler.getJobGroupNames()) {
//                for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
//                    String jobName = jobKey.getName();
//                    String jobGroup = jobKey.getGroup();
//                    //get job's trigger
//                    List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
//                    Date nextFireTime = triggers.get(0).getNextFireTime();
//                    System.out.println("[jobName] : " + jobName + " [groupName] : " + jobGroup + " - " + nextFireTime);
//                }
//            }
//            scheduler.deleteJob(jobDetail.getKey());

        }catch (SchedulerException exception){
            System.out.println(exception.getMessage());
        }

    }

    public void updateEmailRemainderToStudentsInCourse(Course course){
        JobKey jobKey = new JobKey(course.getId()+"courseRemainder", "email-remainder-jobs");
        try{
            scheduler.deleteJob(jobKey);
        }catch (SchedulerException e){
            System.out.println(e.getMessage());
        }
        scheduleEmailRemainderToStudentsInCourse(course);
    }

    private JobDetail buildJobDetail(JobDataMap jobDataMap, String jobId, String jobGroup, String jobDescription){

        return JobBuilder.newJob(EmailRemainderToAllStudentsInCourseJob.class)
                .withIdentity(jobId, jobGroup)
                .withDescription(jobDescription)
                .usingJobData(jobDataMap)
                .storeDurably(false)    //delete job after execution
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, LocalDateTime dateTime,
                                    String triggerGroup, String description){
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), triggerGroup)
                .withDescription(description)
                .startAt(Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }


    private JobDetail buildJobDetail(EmailDetails emailDetails) {

        JobDataMap jobDataMap = new JobDataMap();
//        jobDataMap.put("emailDetails", emailDetails);
        jobDataMap.put("msgBody", emailDetails.getMsgBody());
        jobDataMap.put("subject", emailDetails.getSubject());
        jobDataMap.put("attachment", emailDetails.getAttachment());
        jobDataMap.put("sender", emailDetails.getSender());
        jobDataMap.put("recipients", emailDetails.getRecipients());

        return JobBuilder.newJob(EmailJob.class)
                .withIdentity(UUID.randomUUID().toString(), "email-jobs")
                .withDescription("Send Email Job")
                .usingJobData(jobDataMap)
                .storeDurably(false)
                .build();
    }

    private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime date) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "email-triggers")
                .withDescription("Send Email Trigger")
                .startAt(Date.from(date.toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }

}

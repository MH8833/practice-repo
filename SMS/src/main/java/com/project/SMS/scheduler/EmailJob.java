package com.project.SMS.scheduler;

import com.project.SMS.dto.EmailDetails;
import com.project.SMS.service.EmailService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import org.quartz.JobExecutionContext;

import java.util.List;

@Component
public class EmailJob extends QuartzJobBean {

    @Autowired
    private EmailService emailService;

    @Override
    @SuppressWarnings("unchecked")
    public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException{
//        EmailDetails emailDetails = (EmailDetails) jobExecutionContext.getMergedJobDataMap().get("emailDetails");
        JobDataMap jobMap = jobExecutionContext.getMergedJobDataMap();
        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setMsgBody( jobMap.getString("msgBody"));
        emailDetails.setSubject(jobMap.getString("subject"));
        emailDetails.setAttachment( jobMap.getString("attachment"));
        emailDetails.setSender( jobMap.getString("sender"));
        emailDetails.setRecipients( (List<String>) jobMap.get("recipients"));

        emailService.sendEmail(emailDetails);
//        System.out.println( emailService.sendEmail(emailDetails) );
    }

}

package com.project.SMS.scheduler;

import com.project.SMS.dto.EmailDetails;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

//IMPROVE: review if necessary
public class SchedulerEmailData {

    @NotNull
    private EmailDetails emailDetails;

    @NotNull
    private LocalDateTime dateTime;

    @NotNull
    private ZoneId timeZone;

    public SchedulerEmailData(EmailDetails emailDetails, LocalDateTime dateTime, ZoneId timeZone) {
        this.emailDetails = emailDetails;
        this.dateTime = dateTime;
        this.timeZone = timeZone;
    }

    public SchedulerEmailData(EmailDetails emailDetails, LocalDateTime dateTime) {
        this.emailDetails = emailDetails;
        this.dateTime = dateTime;
        this.timeZone = ZoneId.systemDefault(); //IMPROVE: set the timezone manually?
    }

    public EmailDetails getEmailDetails() {
        return emailDetails;
    }

    public void setEmailDetails(EmailDetails emailDetails) {
        this.emailDetails = emailDetails;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public ZoneId getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(ZoneId timeZone) {
        this.timeZone = timeZone;
    }
}

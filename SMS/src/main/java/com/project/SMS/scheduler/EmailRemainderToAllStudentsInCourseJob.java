package com.project.SMS.scheduler;

import com.project.SMS.dto.EmailDetails;
import com.project.SMS.model.Course;
import com.project.SMS.model.Student;
import com.project.SMS.service.CourseService;
import com.project.SMS.service.EmailService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmailRemainderToAllStudentsInCourseJob extends QuartzJobBean {

//IMPROVE: checar si puedo agregar aqui los metodos para generar los details y triggers, para solo mandar llamar
//desde el "controlador" para sacar ese tipo de cosas de esa clase.
    //talcez metodos statico y/o las variables de grupo, descripciones y esos datos de el jobDetails ponerlos como
    //constantes dentro de esta clase, o talvez en otro lugar

    // dijo algo de clase abstracta para que los jobs tengan que implementar este "executeInternal" y el/los otros metodos
    // que se neceitarian para poder soamente ser utilizados igual por el controlador.


    @Autowired
    private EmailService emailService;

    @Autowired
    private CourseService courseService;

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap dataMap = jobExecutionContext.getMergedJobDataMap();
        Long courseId = dataMap.getLong("courseId");

        Course course = courseService.getById(courseId);

        List<Student> students = course.getStudents();
        if(students == null || students.isEmpty()){
            return;
        }
        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setMsgBody(course.toStringForEmail());
        emailDetails.setSubject("Reminder. Your course "+course.getName()+" starts tomorrow");

        for(Student student : students){
            emailDetails.addRecipient(student.getUser().getEmail());
        }

        emailService.sendEmail(emailDetails);

    }
}

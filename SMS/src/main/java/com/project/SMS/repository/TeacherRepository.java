package com.project.SMS.repository;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    @Query("SELECT s FROM Teacher s WHERE s.user = ?1")
    Optional<Teacher> findByTeacherByUser(User user);

//    @Query("SELECT s FROM Teacher s INNER JOIN user ON s.n")
//    Optional<Teacher> findByTeacherByUserName(String username);

}

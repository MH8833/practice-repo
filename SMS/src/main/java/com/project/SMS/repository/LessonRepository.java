package com.project.SMS.repository;

import com.project.SMS.model.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonRepository extends JpaRepository<Lesson, Long > {
}

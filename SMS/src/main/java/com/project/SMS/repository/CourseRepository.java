package com.project.SMS.repository;

import com.project.SMS.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

//    @Query("SELECT s FROM course_students s WHERE s.student_id = ?1")
//    public List<Course> getCoursesByStudentId(Long studentId);
}

package com.project.SMS.repository;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT s FROM Student s WHERE s.user = ?1")
    Optional<Student> findByStudentByUser(User user);
}

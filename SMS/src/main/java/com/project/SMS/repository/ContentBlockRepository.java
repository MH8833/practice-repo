package com.project.SMS.repository;

import com.project.SMS.UserManagement.User;
import com.project.SMS.model.ContentBlock;
import com.project.SMS.model.Course;
import com.project.SMS.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContentBlockRepository extends JpaRepository<ContentBlock, Long> {

    @Query("SELECT s FROM ContentBlock s WHERE s.id = ?1")
    Optional<ContentBlock> findContentBLockById(Long id);

    @Query("SELECT c FROM ContentBlock c WHERE c.course = ?1 AND c.id = ?2")
    Optional<ContentBlock> getByCourseId(Course course, Long blockId);
}

package com.project.SMS.exception;

import com.project.SMS.service.FileStorageService;

public class FileStorageException extends RuntimeException{

    public FileStorageException(String message){
        super(message);
    }

    public FileStorageException(String message, Throwable cause){
        super(message, cause);
    }
}

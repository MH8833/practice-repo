package com.project.SMS.UserManagement;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query("SELECT u FROM Role u WHERE u.roleName = 'ADMIN'")
    Optional<Role> findAdminRole();

    @Query("SELECT u FROM Role u WHERE u.roleName = 'TEACHER'")
    Optional<Role> findTeacherRole();

    @Query("SELECT u FROM Role u WHERE u.roleName = 'STUDENT'")
    Optional<Role> findStudentRole();


}

package com.project.SMS.UserManagement;

import com.project.SMS.exception.DuplicatedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;


    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public void registerUser(User user) throws DuplicatedUserException{
        //TODO: validate data

        Optional<User> optionalUser = userRepository.findUserByUserName(user.getUserName());

        if(optionalUser.isPresent()){
            //USERNAME IS TAKEN
            throw new DuplicatedUserException();
        }


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setActive(true);
        user.setLastSignIn(LocalDate.now());
        user.setSignInDate(LocalDateTime.now());

        userRepository.save(user);
    }

//    public Optional<User> getUserByUserName(String username){
//        return userRepository.findUserByUserName(username);
//    }

    public User getUserByUserName(String username){
        return userRepository.findUserByUserName(username)
                    .orElseThrow(() -> new NoSuchElementException("User with username: "+username+" not found"));
    }


}

package com.project.SMS.UserManagement;

import com.project.SMS.UserManagement.Role;
import com.project.SMS.dto.Roles;
import com.project.SMS.model.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
//    @Column(unique = true)
    private String email;
    private LocalDateTime signInDate;
    private LocalDate lastSignIn;
    private boolean active = true;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
                joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    private List<Role> roles;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Student student;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Teacher teacher;

//    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
//    @PrimaryKeyJoinColumn
//    private Teacher teacher;

    public User() {
    }

    public User(Long id, String userName, String password, String firstName, String lastName, String email,
                LocalDateTime signInDate, LocalDate lastSignIn, boolean active, List<Role> roles) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.signInDate = signInDate;
        this.lastSignIn = lastSignIn;
        this.active = active;
        this.roles = roles;
    }

    public User(String userName, String password, String firstName, String lastName, String email,
                LocalDateTime signInDate, LocalDate lastSignIn, boolean active, List<Role> roles) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.signInDate = signInDate;
        this.lastSignIn = lastSignIn;
        this.active = active;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public LocalDateTime getSignInDate() {
        return signInDate;
    }

    public void setSignInDate(LocalDateTime signInDate) {
        this.signInDate = signInDate;
    }

    public LocalDate getLastSignIn() {
        return lastSignIn;
    }

    public void setLastSignIn(LocalDate lastSignIn) {
        this.lastSignIn = lastSignIn;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        if(this.student != null && this.student.equals(student))
            return;
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        if(this.teacher != null && this.teacher.equals(teacher))
            return;
        this.teacher = teacher;
    }

    public boolean isStudent(){
        Role studentRole = new Role(Roles.STUDENT.toString());
        return this.getRoles().contains(studentRole);
    }

    public boolean isTeacher(){
        Role teacherRole = new Role(Roles.TEACHER.toString());
        return this.getRoles().contains(teacherRole);
    }

    public boolean isAdmin(){
        Role adminRole = new Role(Roles.ADMIN.toString());
        return this.getRoles().contains(adminRole);
    }
}
